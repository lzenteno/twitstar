require('app-module-path').addPath(__dirname);

const bodyParser = require("body-parser");
const cors = require("cors");
const methodOverride = require("method-override");
const express = require("express");
const app = express();

app.use(methodOverride());
app.use(cors());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(require("routes/app.routes"));

//creates simple http server for health check
const http = require('http');
const port = process.env.PORT || 8080;

const server = http.createServer(app);

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${port}`);
});


