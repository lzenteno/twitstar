const express = require('express');
const Router = express.Router();
const Twit = require('twit')
const {
  getCommonFollowers,
  getUserById
} = require('./../services/followers')
const { asyncForEach } = require('./../commons')

const client = new Twit({
  consumer_key: 'IpJSLKvRaEsKwXVVwWRuAyUwY',
  consumer_secret: 'mubNn1LR5XIX6bf56LhaCGp7rLqWKfqlZhUXgf968RCAHmejkA',
  access_token: '595639643-yN8NXCVUADm99oXlwT9qwgnON9GhXpNrzFyq3rp2',
  access_token_secret: 'mGMHTw9efJOJXVS6WGzOlN8VEuXS9Wz4auTMn0b6Yy4Gr'
})


Router.get('/followers', async (request, response) => {

  const { first_user, second_user } = request.query

  const url = 'https://api.twitter.com/1.1/followers/ids.json'
  const first = {
    screen_name: first_user
  }

  const firstPromise = new Promise((resolve, reject) => {
    client.get(url, first, function(error, followers, res) {
      console.log('error >', error)
      if (!error) {
        resolve(followers)
      }
    })
  })

  const second = {
    screen_name: second_user
  }

  const secondPromise = new Promise((resolve, reject) => {
    client.get(url, second, function(error, followers, res) {
      console.log('error >', error)
      if (!error) {
        resolve(followers)
      }
    })
  })

  Promise.all([firstPromise, secondPromise])
    .then(async (responsesList) => {
      const list = getCommonFollowers(responsesList[0].ids, responsesList[1].ids)
      const users = []
      await asyncForEach(list, async id => {
        console.log('item > ', id)
        users.push(await getUserById(id))
      })

      response.send({
        common_followers: users
      })
    })
    .catch(errors => {
      response.status(500).send({
        errors
      })
    })

})

Router.get('/friends', async (request, response) => {

  const { first_user, second_user } = request.query

  const url = 'https://api.twitter.com/1.1/friends/list.json'
  const first = {
    screen_name: first_user
  }

  const firstPromise = new Promise((resolve, reject) => {
    client.get(url, first, function(error, followers, res) {
      console.log('error >', error)
      if (!error) {
        resolve(followers)
      }
    })
  })

  const second = {
    screen_name: second_user
  }

  const secondPromise = new Promise((resolve, reject) => {
    client.get(url, second, function(error, followers, res) {
      console.log('error >', error)
      if (!error) {
        resolve(followers)
      }
    })
  })

  Promise.all([firstPromise, secondPromise])
    .then(responsesList => {
      response.send({
        firstResponse: responsesList[0],
        secondResponse: responsesList[1]
      })
    })
    .catch(errors => {
      response.status(500).send({
        errors
      })
    })

})

Router.get('/graph', async (request, response) => {
})

module.exports = Router;
